--[[
Intersection tests for a number of shapes
--]]

local wintersect = {}


-- Checks if the point is within the rectangle
-- rectangle coords are left, top, right, botton
function wintersect.point_rect_intersect(px, py, rl, rt, rr, rb)
  return (px > rl and px < rr) and (py > rt and py < rb)
end




-- Checks if two line segments intersect. Line segments are given in form of ({x,y},{x,y}, {x,y},{x,y}).
-- IS NOT TESTED PROPERLY!
function wintersect.line_line_intersects(l1p1, l1p2, l2p1, l2p2)
  local function checkDir(pt1, pt2, pt3) return math.sign(((pt2.x - pt1.x) * (pt3.y - pt1.y)) - ((pt3.x - pt1.x) * (pt2.y - pt1.y))) end
  return (checkDir(l1p1, l1p2, l2p1) ~= checkDir(l1p1, l1p2, l2p2)) and (checkDir(l2p1, l2p2, l1p1) ~= checkDir(l2p1, l2p2, l1p2))
end

-- Checks if two lines intersect (or line segments if seg is true)
-- Lines are given as four numbers (two coordinates)
-- IS NOT TESTED PROPERLY!
function wintersect.line_line_intersectPoint(l1p1x, l1p1y, l1p2x, l1p2y, l2p1x, l2p1y, l2p2x, l2p2y, seg1, seg2)
  local a1, b1, a2, b2 = l1p2y - l1p1y, l1p1x - l1p2x, l2p2y - l2p1y, l2p1x - l2p2x
  local c1, c2 = a1 * l1p1x + b1 * l1p1y, a2 * l2p1x + b2 * l2p1y
  local det, x, y = a1 * b2 - a2 * b1
  if det == 0 then return false, "The lines are parallel." end
  x, y = (b2 * c1 - b1 * c2) / det, (a1 * c2 - a2 * c1) / det
  if seg1 or seg2 then
    local min, max = math.min, math.max
    if seg1 and not (min(l1p1x, l1p2x) <= x and x <= max(l1p1x, l1p2x) and min(l1p1y, l1p2y) <= y and y <= max(l1p1y, l1p2y)) or
    seg2 and not (min(l2p1x, l2p2x) <= x and x <= max(l2p1x, l2p2x) and min(l2p1y, l2p2y) <= y and y <= max(l2p1y, l2p2y)) then
      return false, "The lines don't intersect."
    end
  end
  return x, y
end



return wintersect