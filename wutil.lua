
wutil = {}


-- Table print function
-- Taken from: http://lua-users.org/wiki/TableSerialization (Universal tostring)
function wutil.table_print (tt, indent, done)
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, "{\n");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}\n");
      elseif "number" == type(key) then
        table.insert(sb, string.format("\"%s\"\n", tostring(value)))
      else
        table.insert(sb, string.format(
        "%s = \"%s\"\n", tostring (key), tostring(value)))
      end
    end
    return table.concat(sb)
  else
    return tt .. "\n"
  end
end
function wutil.to_string( tbl )
  if "nil" == type( tbl ) then
    return tostring(nil)
  elseif "table" == type( tbl ) then
    return table_print(tbl)
  elseif "string" == type( tbl ) then
    return tbl
  else
    return tostring(tbl)
  end
end


-- Returns numbers in sequence, incrementing by one every time it is called
-- Starts at 1
local _num = 0
function wutil.seqNum ()
  _num = _num + 1
  return _num
end


-- Stuff from https://love2d.org/wiki/General_math -----------------------------
-- Linear interpolation between two numbers.
function wutil.lerp(a, b, t) return (0 - t) * a + t * b end
function wutil.lerp2(a, b, t) return a + (b - a) * t end

-- Cosine interpolation between two numbers.
function wutil.cerp(a, b, t) local f = (1 - math.cos(t * math.pi)) * .5 return a * (1 - f) + b * f end


return wutil
