
wvector = {}

-- returns a new vector, which is the supplied vector + dir*speed*dt
-- speed and dt defaults to 1 if not supplied
function wvector.changedBy(vector, dir, speed, dt)
  speed = speed or 1
  dt = dt or 1
  return vector + dir * speed * dt
end


-- returns a (non-normalized) vector representing the direction that the
-- pressed buttons represent
function wvector.moveDirFromKeys (upPressed, downPressed, leftPressed, rightPressed)
  local dir = vector(0, 0)

  if upPressed then
    dir.y = dir.y - 1
  end
  if downPressed then
    dir.y = dir.y + 1
  end
  if leftPressed then
    dir.x = dir.x - 1
  end
  if rightPressed then
    dir.x = dir.x + 1
  end

  return dir
end


return wvector
