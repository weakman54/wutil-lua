
wmath = {}

-- Rounds to numDecimalPlaces
-- rounds to 10s, 100s and so on if numDecimalPlaces is negative
-- taken from http://lua-users.org/wiki/SimpleRound
function wmath.round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end



-- Stuff from https://love2d.org/wiki/General_math -----------------------------
-- Returns the distance between two points.
function wmath.dist(x1, y1, x2, y2) return ((x2 - x1)^2 + (y2 - y1)^2)^0.5 end
-- Distance between two 3D points:
function wmath.dist3(x1, y1, z1, x2, y2, z2) return ((x2 - x1)^2 + (y2 - y1)^2 + (z2 - z1)^2)^0.5 end


-- Averages an arbitrary number of angles (in radians).
function wmath.averageAngles(...)
  local x, y = 0, 0
  for i = 1, select('#', ...) do local a = select(i, ...) x, y = x + math.cos(a), y + math.sin(a) end
  return math.atan2(y, x)
end


-- Returns the angle between two points.
function wmath.angle(x1, y1, x2, y2) return math.atan2(y2 - y1, x2 - x1) end


-- Returns the closest multiple of 'size' (defaulting to 10).
function wmath.multiple(n, size) size = size or 10 return math.round(n / size) * size end


-- Clamps a number to within a certain range.
function wmath.clamp(low, n, high) return math.min(math.max(low, n), high) end


-- Normalize two numbers.
function wmath.normalize(x, y)
  local l = (x * x + y * y)^.5
  if l == 0 then return 0, 0, 0
  else return x / l, y / l, l end
end


-- Returns 1 if number is positive, -1 if it's negative, or 0 if it's 0.
function wmath.sign(n) return n > 0 and 1 or n < 0 and - 1 or 0 end


return wmath
