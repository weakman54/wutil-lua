
wlove = {}

-- all_callbacks calculation gotten from hump.gamestate
local all_callbacks = { 'draw', 'errhand', 'update' }
for k in pairs(love.handlers) do
  table.insert(all_callbacks, k)
end
-- Checks through all callbacks defined by lua, if they are not implemented
-- in obj, an empty function is inserted.
-- This is used to generalize objects so that they can be put into an array
-- together without stuff breaking for example.
function wlove.attachEmptyLoveCallbacks (obj)
  for _, callback in pairs(all_callbacks) do
    if obj[callback] == nil then
      obj[callback] = function() end
    end
  end
end


return wlove
